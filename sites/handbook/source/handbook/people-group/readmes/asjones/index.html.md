---
layout: markdown_page
title: "Ashley Jones's README"
description: "This page is Ashley Jones's readme and is intended to be helpful when interacting with her."
---

This page is intended to help others understand what it might be like to work with me, especially people who haven’t worked with me before. 

It’s also a well-intentioned effort at building some trust by being intentionally vulnerable, and to share my ideas of a good working relationship to reduce the anxiety of people who might be on my team.

Please feel free to contribute to this page by creating a merge request. 

## About me

I have been working for GitLab since September of 2019 as a Candidate Experience Specialist, being promoted to the Candidate Experience Team Lead in May of 2020. I live in Lansing, Michigan (EST) and have two dogs, Chandler and Monica, that make an appearance in the #dog slack channel every once in a while. 

I am a true crime junkie and will happily talk about it at any time. I enjoy kayaking or spending time at the beach/pool with a drink in my hand. Also, I am a Disney maniac and a Potterhead to add (Slytherin, obviously).

## Availability 

My typical working hours are 8:00 am to 4:00 pm est as noted in my Google hours, however, I am always available to adjust my schedule or attend meetings outside of those hours if needed and notice is given.

## How I Operate

Once I have something to do, it is on my mind until I have completed it so I will do it immediately, if not the first thing the next day. I do not expect that of everyone else as I love how GitLab is async and appreciate the same respect. I assume everyone prioritizes tasks based on company, department, and team needs. 

You have my trust and respect until you give me a reason to doubt it. I believe everyone works at GitLab for a reason and is 100% capable of doing their jobs. 

If you need my assistance or for me to contribute to an issue/MR/email/slack discussion, please tag me and specifically call out what you need from me. If there is not a specific callout, I will assume it’s an 'FYI' notification and will read when I have the chance. 

No topic is off the table or prohibited to discuss in my eyes. If you want to talk about religion, relationships, family, personal things, or anything work-related - I am all ears and open for a mature, adult conversation. 

I have a very good memory but it’s definitely not perfect. Once I complete a task, I essentially "dump" it from my brain. I may need you to remind me of the background details during the discussion at times.

## Meetings

Please share an agenda beforehand. I like to be able to come prepared to discuss the topics and have issues/MRs or announcements pulled up for reference. This also helps me cut down on personal chit chat, if necessary. I will talk with you about your personal life, animals, family, etc. for as long as you’ll let me and then we will run out of time. If I know we only have 1 topic to cover, I’ll chat it up. But if I know you have 4-5 items to cover with me, I will keep it short and sweet to make sure I’m making good use of your time.

## Best Way to Contact Me

I do not have email on my phone. If there is something important outside of my working hours that needs my attention, please @ me or DM me in Slack.
